package com.bibao.boot.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AOPTest {
	private ClassPathXmlApplicationContext actx;
	
	@Before
	public void setUp() throws Exception {
		actx = new ClassPathXmlApplicationContext("config.xml");
	}

	@After
	public void tearDown() throws Exception {
		actx.close();
	}
	
	@Test
	public void testGetFullName() {
		MyService service = actx.getBean("serviceProxy", MyService.class);
		assertTrue(service instanceof MyService);
		assertNotSame(service.getClass(), MyService.class);
		String fullName = service.getFullName("Bruce", "Lee");
		assertEquals("Hello John Bush, welcome to Spring AOP!", fullName);
		MyService originalService = actx.getBean("service", MyService.class);
		assertSame(originalService.getClass(), MyService.class);
		assertEquals("Alice Zhang", originalService.getFullName("Alice", "Zhang"));
	}

	@Test
	public void testDummy() {
		MyService service = actx.getBean("serviceProxy", MyService.class);
		service.dummy();
	}
}
