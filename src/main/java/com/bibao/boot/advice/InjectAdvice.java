package com.bibao.boot.advice;

import java.util.Arrays;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class InjectAdvice {
	
	@Around("execution(* com.bibao.boot.service.MyService.getFullName(..))")
	public Object injectAround(ProceedingJoinPoint pjp) throws Throwable {
		Object[] args = pjp.getArgs();
		args[0] = "Bob";
		args[1] = "Zhu";
		Object result = pjp.proceed(args);
		return "Hello " + result + ", welcome to AspectJ!";
	}
	
	@Before("execution(* com.bibao.boot.service.MyService.printArray(..))")
	public void injectBefore(JoinPoint joinPoint) {
		int[] array = (int[]) joinPoint.getArgs()[0];
		Arrays.sort(array);
	}
	
	@SuppressWarnings("unchecked")
	@AfterReturning(
		pointcut = "execution(* com.bibao.boot.service.MyService.generateList(..))",
		returning = "returnValue"
	)
	public void injectAfter(JoinPoint joinPoint, Object returnValue) {
		List<String> list = (List<String>)returnValue;
		list.add("C");
	}
}
