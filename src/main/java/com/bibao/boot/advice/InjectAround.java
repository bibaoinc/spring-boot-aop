package com.bibao.boot.advice;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class InjectAround implements MethodInterceptor {

	@Override
	public Object invoke(MethodInvocation method) throws Throwable {
		Object[] args = method.getArguments();
		args[0] = "John";
		args[1] = "Bush";
		Object result = method.proceed();
		result = "Hello " + result + ", welcome to Spring AOP!";
		return result;
	}

}
