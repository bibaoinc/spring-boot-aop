package com.bibao.boot.service;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class MyService {
	public String getFullName(String firstName, String lastName) {
		return firstName + " " + lastName;
	}
	
	public void dummy() {
		
	}
	
	public String printArray(int[] array) {
		int len = array.length;
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int i=0; i<len-1; i++) {
			sb.append(array[i] + ",");
		}
		sb.append(array[len-1] + "]");
		return sb.toString();
	}
	
	public List<String> generateList(List<String> list) {
		list.add("A");
		list.add("B");
		return list;
	}
}
